const fetch = require('node-fetch');
const assert = require('assert').strict;

const hostname = process.env.HOSTNAME || 'localhost';
const port = process.env.PORT || 8080;

async function main() {
    const res = await fetch(`http://${hostname}:${port}`);
    assert.equal(res.ok, true);
    const body = await res.text();
    assert.equal(body, "Hello World!");
    console.log('All tests passed!');
}

main().catch(err => {
    console.error(err);
    process.exit(1);
});